import os

def continuar():
    print()
    input('presione una tecla para continuar ...')
    os.system('cls')

def menu():
    print('1) Cargar productos')
    print('2) Mostrar el listado de Productos')
    print('3) ')
    print('4) ')
    print('5) ')
    print('6) fin del programa')
   
    eleccion = int(input('Elija una opción: '))
    while not((eleccion >= 1) and (eleccion <= 6)):
        eleccion = int(input('Elija una opción: '))
    os.system('cls')
    return eleccion

def leerPrecio():
    precio = float(input('precio: '))
    while ((precio<0)):
        precio = float(input('Precio: '))
    return precio


def leerStock():
    stock = int(input('stock: '))
    while ((stock<0)):
        stock = int(input('stock: '))
    return stock


def mostrar(diccionario):
    print('Listado de productos')
    for clave, valor in diccionario.items():
        print(clave,valor)

def leerProductos():
    print('Cargar Lista de productos')
    productos = {}
    cod = -1
    while (cod != 0):
        cod = int(input('ingrese codigo (cero para finalizar): '))
        if cod != 0: 
            if cod not in productos:    
                descripcion = input('descripcion: ')
                precio = leerPrecio()
                stock = leerStock()
                productos[cod] = [descripcion,precio,stock]
                print('agregado correctamente')
            else:
                print('el codigo  ya existe')

    return productos








def salir():
    print('Fin del programa...')

#principal
opcion = 0

os.system('cls')
while (opcion != 6):
    opcion = menu()
    if opcion == 1:
        productos = leerProductos()
    elif opcion == 2:
        mostrar(productos)
  
    elif (opcion == 6):        
        salir()
    continuar()
